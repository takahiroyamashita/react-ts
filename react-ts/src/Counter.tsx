import * as React from "react";

type State = {
  count: number
}

export default class Counter extends React.Component<any, State> {

  constructor(props: any) {
    super(props);
    this.state = {
      count: 0
    };
  }

  increase(): void {
    const count = this.state.count;
    this.setState({
      count: count + 1
    });
  }

  render(): React.ReactNode {
    return (
      <div>
        <input type="text" value={this.state.count} />
        <input type="button" value="increase" onClick={this.increase.bind(this)} />
      </div>
    );
  }
}
